# Expresiones Regulares

Regex
=====

## Presentación

Las Expresiones Regulares son una herramienta de búsqueda y manipulación de cadenas de caracteres increíblemente potente presente en **todos** los lenguajes de programación. Este curso busca llevar al alumno a entenderlas y darles un uso correcto dentro de sus diferentes aplicaciones.

Algunos puntos de este temario asumen un uso intermedio de la CLI, por lo que se recomienda el curso de "Línea de Comandos".

## Temario

1. Qué es y para qué sirven las expresiones regulares, por ejemplo `/^([a-z\.\+]{4,30})@([a-z\.]+)\.([\w]{2,5})$/`
1. Notas sobre el curso
1. El caracter `.` e introducción a los caracteres especiales y su escapado
1. Los delimitadores numéricos: `+`, `*`, `?`
1. Los contadores `{1,4}`
1. Las clases predefinidas `\w`, `\d`, `\s` …
1. Las clases construidas `[a-zA-Z0-9]`
1. Not `^`, su uso y sus peligros
1. El caso de `?` como delimitador
1. Principio (`$`) y final de línea (`^`)
1. Expresiones comunes:
  1. mails
  1. teléfonos
  1. logs
  1. nombres
  1. locaciones
    1. [what three words](https://what3words.com/)
1. Búsqueda y reemplazo
1. Procesadores de texto
1. `grep` y `find` desde consola
1. Regex en
  1. PHP
  1. Javascript
  1. Python
  1. Perl (aunque se burlen)

# ¿Qué son las expresiones regulares?

Las expresiones regulares son patrones de caracteres que te permiten ir seleccionando o descartando datos en un archivo de texto como por ejemplo csv, o en una línea o un input, según coincidan o nó con este patrón.

Prácticamente todos los lenguajes de programación tienen librerías o módulos para manejar expresiones regulares.

Las expresiones regulares pueden ser muy complejas pero no son nada difíciles de entender.

A través de este curso, sin tecnicismos y con ejemplos puntuales, vamos a aprender a utilizarlas para que sean esa herramienta que siempre nos ayude, y sea la primera para solucionar problemas de grandes cantidades de datos en string.

----------------------------------------
Expresiones Regulares, son patrones en los que definimos que cadenas de caracteres entran o no entran en el patrón diseñado.
Además de ser útiles para quedarnos con parte de la información que necesitamos y descartamos la que no.
-----------------------------------------------

# Aplicaciones de las expresiones regulares

Buscar e investigar sobre Expresiones Regulares puede ser muy intimidante.
`/^(.){5}\w?[a-Z|A-Z|0-9]$/ig`
En serio pueden parecer muy, muy raras; pero la verdad es que no lo son.

En esta clase aprenderás, para qué te puede servir el saber usar bien las Expresiones Regulares; y es, en pocas palabras, para buscar.

# Introducción al lenguaje de expresiones regulares

Con las expresiones regulares vamos a solucionar problemas reales, problemas del día a día.

¿Qué pasa si queremos buscar en un texto (txt, csv, log, cualquiera), todos los números de teléfonos que hay?
Tendríamos que considerar por ejemplo, que un teléfono de México serían 10 dígitos; hay quienes los separan con guión, hay quienes los separan con puntos, hay quienes no los separan sino que tienen los 10 dígitos exactos, y este patrón puede cambiar para otros países.

Esto mismo sucede con números de tarjetas de crédito, códigos postales, dirección de correos, formatos de fechas o montos, etc.

https://regex101.com/ <b>Practica online</b>

# El caracter (.)

Qué es un archivo de texto, por ejemplo un CSV?
¿Qué es una cadena de caracteres?

Cada espacio en una cadena de texto se llena con un caracter, esto lo necesitamos tener perfectamente claro para comenzar a trabajar con expresiones regulares

Abriremos nuestro editor, qué en este curso recomendamos ATOM, vamos a presionar CTRL + F y podremos buscar por match idénticos.

------------------------------------
Cadena de Caracteres: Es un carácter ASCII generalmente, seguido de otro carácter y de otro. No todos son visibles, el espacio por ejemplo. Cada carácter es un carácter.

El caracter (.): Encuentrame todo lo que sea un carácter

Con Sublime Text también pueden hacer las búsquedas con expresiones regulares dando igual al asterisco cuando se abre el buscador. Imagino que con VSCode debe ser igual, pero no sé
---------------------------------------------

## nota

Dicen que cuando tienes un problema y lo intentas solucionar con Expresiones regulares tienes dos problemas.

Falso.

Aunque las expresiones regulares pueden ser un poco intimidantes en un principio, son un herramienta que todo desarrollador –independientemente del lenguaje de programación de su preferencia– debe de traer SIEMPRE en la mochila.

Las expresiones regulares, fuera de las definiciones teóricas, se basan en un lenguaje aunque ha evolucionado desde hace más de medio siglo, sigue siendo el mismo desde los 80 Y TODO EL MUNDO LOS USA.

Sirven para crear patrones de búsqueda de texto y las herramientas (y lenguajes) que las utilizan se usan para tratar desde grandes volúmenes de datos hasta corroborar que una dirección de email está bien escrita.

Tratar, por ejemplo un archivo de más de 2 millones de líneas y 120 megas en menos de 2 segundos, nada mal, ¿no crees?

No sólo es copiar y pegar lo que parecen ser puntos, líneas, asteriscos, signos de interrogación que parecen tener ningún sentido de stack overflow, sino entender exactamente qué hace cada uno y crear los tuyos.


# Las clases predefinidas y construida

<p><strong>Dígitos:</strong> <ins>\d</ins></p>
<ul>
<li>Encuentra todos los dígitos de 0 a 9.</li>
<li>\d es equivalente a poner [0-9].</li>
<li>Si en vez de \d, usamos por ejemplo [0-2] nos encontrará solamente los dígitos de 0 a 2.</li>
<li>Podemos usar “\D” para encontrar justo lo contrario, todo lo que no son dígitos.</li>
</ul>
<p><strong>Palabras:</strong> <ins>\w</ins></p>
<ul>
<li>Encuentra todo lo que puede ser parte de una palabra, tanto letras (minúsculas o mayúsculas) como números.</li>
<li>\w es equivalente a poner [a-zA-Z0-9_].</li>
<li>Si en vez de \w, usamos por ejemplo [a-zA-Z] nos encontrará solamente las letras.</li>
<li>Podemos usar “\W” para encontrar justo lo contrario, todos los caracteres que no son parte de palabras.</li>
</ul>
<p><strong>Espacios:</strong> <ins>\s</ins></p>
<ul>
<li>Encuentra todos los espacios (los saltos de línea también son espacios).</li>
<li>Podemos usar “\S” para encontrar justo lo contrario, todo lo que no son espacios.</li>
</ul>

\w - caracteres de palabras
\d - dígitos
\s - espacios/invisibles en blanco o tabs
[0-9] - equivale \d
[0-9a-zA-Z] - equivalente a \w

# Los delimitadores: +, *, ?

* greedy - todo
+ - 1 o más
? - cero o uni

<p><strong>Delimitadores</strong>:</p>
<ul>
<li>(*) : Cero o más veces</li>
<li>(?): Cero o una sola vez</li>
<li>(+): una o más veces.</li>
</ul>
<p>Aplican al carácter o sentencia que preceden</p>
<ul>
<li><strong>[a-z]?</strong> : Esto es que puede estar <em><strong>una sola vez</strong></em> o <strong><em>no estar</em></strong> una letra minuscula de la <strong>(a)</strong> a la <strong>(z)</strong>.</li>
<li><strong>\d</strong>*: Esto es que puede estar <strong><em>muchas veces</em></strong> o <strong><em>no estar</em></strong> un <strong>digito</strong>.</li>
<li><strong>\d+</strong>: Esto es que puede estar  <strong><em>muchas veces</em></strong> o <strong><em>una sola vez</em></strong> un <strong>digito</strong>.</li>
</ul>


--------------------------

- ¡Encontré este sitio https://www.debuggex.com/, en el que se puede ver el flujo lógico de las expresiones regulares!
Para este patrón:

		\d*[a-z]?s\d+

![image ver](images/ex1.jpg)
![image ver](images/ex2.jpg)
![image ver](images/ex3.jpg)

--------------------------------------

- Hice estos apuntes de lo que entendí a mi parecer hasta ahora y pude probar en https://regex101.com/
Espero que a alguien como yo, que vio el vídeo como 3 veces para comprender el uso de los delimitadores, le pueda ayudar:

![image ver](images/ex4.jpg)
![image ver](images/ex5.jpg)
![image ver](images/ex6.jpg)
![image ver](images/ex7.png)

-----------------------------------------

<p><code>+ --&gt; 1 o más.</code><br>
Incluye solo si aparece este dato</p>
<p><code>*--&gt; 0 o más</code><br>
No importa en sí, pero estaría bueno que sí lo encontraras sin importar cuantas veces te salgan.</p>
<p><code>? --&gt; 0 o 1 vez.</code><br>
No importa si no lo encuentras, pero si lo haces, SOLO ME SIRVE UNA VEZ, el resto está de más.</p>

----------------------------------------------------

# Los contadores {1,4}

Lo que vamos a aprender en esta clase es comenzar a generalizar nuestras búsquedas, a ser específicos cubriendo grandes cantidades de caracteres sin tener que escribir de forma repetitiva como sería poner por ejemplo “\d\d\d\d\d\d\d\d…”

<p><strong>Contadores</strong>:<br>
Aplicando a un carácter que lo preceda se puede colocar entre llaves de esta forma, para indicarle que busque la cantidad deseada de caracteres.</p>
<p>{<strong>Cota inferiror</strong>, <strong>Cota superior</strong>}</p>
<p><strong>Ejemplo:</strong></p>
<ul>
<li><strong>\d{0,2}:</strong> Esto buscara 0, 1, 2 dígitos</li>
<li><strong>\d{2,2}:</strong> Esto buscara 2 dígitos</li>
<li><strong>\d{2}:</strong> Esto buscara 2 dígitos</li>
<li><strong>\d{2,}:</strong> Esto buscara 2 o más dígitos.</li>
<li><strong>\d{2,4}:</strong> Esto buscara 2, 3, 4 dígitos.</li>
</ul>

# El caso de (?) como delimitador

- El ? indica al patrón que encuentre las coincidencias de manera rápida (o greedy); es decir, devolviendo el resultado más pequeño que haga match hasta donde se encuentra el delimitador, y esto lo haga tantas veces como sea posible dentro de la cadena.

-------------------------------------------
<p><strong>Delimitador ?:</strong><br>
Los matches los hace lo más pequeños posibles.<br>
Es decir: Haz el match, pero los divides en grupos pequeños.</p>
<p>Ejemplo:</p>
<pre><code>.+?
</code></pre>
<p>Encuentra todos los caracteres y haces matches pequeños.</p>
------------------------------------------
*? Coincide con el elemento anterior cero o más veces, pero el menor número de veces que sea posible.
+? Coincide con el elemento anterior una o más veces, pero el menor número de veces que sea posible.
?? Coincide con el elemento anterior cero o una vez, pero el menor número de veces que sea posible.

**La función de (?) como delimitador conociste justamente en delimitar a la menor cantidad posible de los matches. **
------------------------------------------------
El aporte está genial pero pienso que se lo redactaría mejor diciendo que ? como delimitador se encarga de hacer la mayor cantidad de matches posibles mientras se cumpla la expresión regular. Cada uno lo asimila mejor con diferentes sentencias y tal vez lo mio ayude.

# Not (^), su uso y sus peligros
Este caracter nos permite negar una clase o construir “anticlases”, vamos a llamarlo así, que es: toda la serie de caracteres que no queremos que entren en nuestro resultado de búsqueda.

Para esto definimos una clase, por ejemplo: [ 0-9 ], y la negamos [ ^0-9 ] para buscar todos los caracteres que coincidan con cualquier caracter que no sea 0,1,2,3,4,5,6,7,8 ó 9

-----------------------------------------------------------
Aclarar que el “gorrito” (^) solo funciona como negación cuando está dentro de los corchetes [ ] estando a fuera significa otra cosa jeje en dos vídeos siguiente nos damos cuenta
------------------------------------------------------------
<p><strong>estos</strong> <strong>son</strong> <strong>los</strong> <strong>demas:</strong> 😉</p>
<p><strong>\t</strong> — Representa un tabulador.<br>
<strong>\r</strong> — Representa el “retorno de carro” o “regreso al inicio” o sea el lugar en que la línea vuelve a iniciar.<br>
<strong>\n</strong> — Representa la “nueva línea” el carácter por medio del cual una línea da inicio. Es necesario recordar que en Windows es necesaria una combinación de \r\n para comenzar una nueva línea, mientras que en Unix solamente se usa \n y en Mac_OS clásico se usa solamente \r.<br>
<strong>\a</strong> — Representa una “campana” o “beep” que se produce al imprimir este carácter.<br>
<strong>\e</strong> — Representa la tecla “Esc” o “Escape”<br>
<strong>\f</strong> — Representa un salto de página<br>
<strong>\v</strong> — Representa un tabulador vertical<br>
<strong>\x</strong> — Se utiliza para representar caracteres ASCII o ANSI si conoce su código. De esta forma, si se busca el símbolo de derechos de autor y la fuente en la que se busca utiliza el conjunto de caracteres Latin-1 es posible encontrarlo utilizando “\xA9”.<br>
<strong>\u</strong> — Se utiliza para representar caracteres Unicode si se conoce su código. “\u00A2” representa el símbolo de centavos. No todos los motores de Expresiones Regulares soportan Unicode. El .Net Framework lo hace, pero el EditPad Pro no, por ejemplo.<br>
<strong>\d</strong> — Representa un dígito del 0 al 9.<br>
<strong>\w</strong> — Representa cualquier carácter alfanumérico.<br>
<strong>\s</strong> — Representa un espacio en blanco.<br>
<strong>\D</strong> — Representa cualquier carácter que no sea un dígito del 0 al 9.<br>
<strong>\W</strong> — Representa cualquier carácter no alfanumérico.<br>
<strong>\S</strong> — Representa cualquier carácter que no sea un espacio en blanco.<br>
<strong>\A</strong> — Representa el inicio de la cadena. No un carácter sino una posición.<br>
<strong>\Z</strong> — Representa el final de la cadena. No un carácter sino una posición.<br>
<strong>\b</strong> — Marca la posición de una palabra limitada por espacios en blanco, puntuación o el inicio/final de una cadena.<br>
<strong>\B</strong> — Marca la posición entre dos caracteres alfanuméricos o dos no-alfanuméricos.</p>
-------------------------------------------------------------------

# Reto: Filtrando letras en números telefónicos utilizando negaciones

En el texto siguiente:
555658
56-58-11
56.58.11
56.78-98
65 09 87
76y87r98

Definir un patrón que haga match a todas las líneas excepto a la la última, la que tiene letras.

Es decir, seleccionar todas sin importar el caracter de separación, excepto cuando los números están separados entre sí por letras.

<b>Misolución suin usar negación:</b>

    ([0-9][0-9][\-\. ]?){3,}

<b>Mi solución usando negación:</b>

    (\d{2,2}\W?){3,3}


# Principio (^) y final de linea ($)
Estos dos caracteres indican en qué posición de la línea debe hacerse la búsqueda:
el ^ se utiliza para indicar el principio de línea
el $ se utiliza para indicar final de línea

^ ------------- $

<b>Nota</b>: El match es una linea entera o se descarta

![image ver](images/ex8.png)

# Logs

Las expresiones regulares son muy útiles para encontrar líneas específicas que nos dicen algo muy puntual dentro de los archivos de logs que pueden llegar a tener millones de líneas.

# Teléfonos

![image ver](images/ex9.png)

# URLs

Una de las cosas que más vamos a usar en la vida, seamos frontend o backend, serán directamente dominios o direcciones de internet; ya sea direcciones completas de archivo (una url) o puntualmente dominios para ver si es correcto un mail o no.

    https?:\/\/[\w\-\.]+\.\w{2,6}\/?\S


-------------------------------------------------------------------
<p>TLD -&gt; Top Level domains, dominio de nivel superior. Es la  terminación de una página, puede ser .com, .org, .mx, etc.</p>
<p>Búsqueda:</p>
<pre><code>https?:<span class="hljs-tag">\<span class="hljs-name">/</span></span><span class="hljs-tag">\<span class="hljs-name">/</span><span class="hljs-string">[\w\-\.]</span></span>+<span class="hljs-tag">\<span class="hljs-name">.</span></span><span class="hljs-tag">\<span class="hljs-name">w</span><span class="hljs-string">{2,5}</span></span>
</code></pre>
<p>(esta última parte selecciona a los TLD).<br>
Búsqueda la url completa -&gt;</p>
<pre><code>https?:<span class="hljs-tag">\<span class="hljs-name">/</span></span><span class="hljs-tag">\<span class="hljs-name">/</span><span class="hljs-string">[\w\-\.]</span></span>+<span class="hljs-tag">\<span class="hljs-name">.</span></span><span class="hljs-tag">\<span class="hljs-name">w</span><span class="hljs-string">{2,5}</span></span><span class="hljs-tag">\<span class="hljs-name">/</span></span>?<span class="hljs-tag">\<span class="hljs-name">S*</span></span> 
</code></pre>
<p>No tiene en cuenta a los espacios en blanco.</p>
<p>Búsqueda “para simplificarte la vida” -&gt;</p>
<pre><code>https?\<span class="hljs-keyword">S</span>*
</code></pre>
<p>Es completamente inútil si existen urls no válidas, por ejemplo http-123423312, esta página es básura, ruido, pero con esta búsqueda quedará seleccionada, en un mundo ideal dónde se guardan las urls de la misma forma y sin errores sería idónea, pero dónde existe el error humano puede volverse inútil.</p>
-----------------------------------------------------------------------

# Mails

<pre><code>esto<span class="hljs-selector-class">.es</span><span class="hljs-selector-class">.un</span><span class="hljs-selector-class">.mail</span>+gmail@mail<span class="hljs-selector-class">.com</span>
esto<span class="hljs-selector-class">.es_un</span><span class="hljs-selector-class">.mail</span>@mail<span class="hljs-selector-class">.com</span>
esto<span class="hljs-selector-class">.es_un</span><span class="hljs-selector-class">.mail</span>+complejo@mail<span class="hljs-selector-class">.com</span>
dominio<span class="hljs-selector-class">.com</span>
rodrigo.jimenez@yahoo<span class="hljs-selector-class">.com</span><span class="hljs-selector-class">.mx</span>
ruben@starbucks<span class="hljs-selector-class">.com</span>
esto_no<span class="hljs-variable">$es_email</span>@dominio<span class="hljs-selector-class">.com</span>
no_se_de_internet3@hotmail<span class="hljs-selector-class">.com</span>
</code></pre>
<blockquote>
<p>Patron</p>
</blockquote>
<pre><code>[<span class="hljs-tag">\<span class="hljs-name">w</span></span><span class="hljs-tag">\<span class="hljs-name">.</span></span>_]{5,30}<span class="hljs-tag">\<span class="hljs-name">+</span></span>?<span class="hljs-tag">\<span class="hljs-name">w</span><span class="hljs-string">{0,10}</span></span>@[<span class="hljs-tag">\<span class="hljs-name">w</span></span><span class="hljs-tag">\<span class="hljs-name">-</span></span><span class="hljs-tag">\<span class="hljs-name">.</span></span>]{3,}<span class="hljs-tag">\<span class="hljs-name">.</span></span><span class="hljs-tag">\<span class="hljs-name">w</span><span class="hljs-string">{2,5}</span></span>
</code></pre>
<p>Patron2</p>
<pre>
<code>^[\w\.]+@[\w\-\.]+\.\w{2,5}$</code>
</pre>
<blockquote>
<p>Explicación</p>
</blockquote>
<ul>
<li><code>[\w\._]{5,30} =&gt; que contengan de 5 hasta 30 caracteres alfanuméricos incluyendo _ y el .</code></li>
<li><code>\+? =&gt; puede contener un +</code></li>
<li><code>\w{0,10} =&gt; que contengan de 0 hasta 10 caracteres alfanuméricos</code></li>
<li><code>@ =&gt; que contenga una @</code></li>
<li><code>[\w\-\.]{3,} =&gt; que contengan de 3 o mas caracteres alfanuméricos incluyendo _ y el .</code></li>
<li><code>\. =&gt; que contenga un .</code></li>
<li><code>\w{2,5} =&gt; que contengan de 2 hasta 5</code></li>
</ul>

# Localizaciones

Esta clase nos va a servir para ver unos tips comunes de qué hacer y sobre todo qué no hacer con expresiones regulares, usando como ejemplo datos de posicionamiento en el mapa: latitud y longitud.

---------------------------------------

    -99.205646,19.429707,2275.10
    -99.205581, 19.429652,2275.10
    -99.204654,19.428952,2275.58

    ^\-?\d{1,3}\.\d{1,6},\s?\-?\d{1,3}\.\d{1,6},.*$


    --

    -99 12' 34.08"W, 19 34' 56.98"N
    -34 54' 32.00"E, -3 21' 67.00"S

    ^\-?\d{1,3}\s\d{1,2}'\s\d{1,2}\.\d{2,2}"[WE],\s?\-?\d{1,3}\s\d{1,2}'\s\d{1,2}\.\d{2,2}"[NS]$


    --
    https://map.what3words.com/

    gravel.notes.handbag
    reaming.embeds.rats

    ^[a-z]{3,}\.[a-z]{3,}\.[a-z]{3,}$


# Nombres(?) Reto

Usar expresiones regualres para buscar nombres y apellidos.

- mi solución fue esta, ver la primera letra mayuscula con sus acentos `[A-ZÓ]`, luego una letra minuscula `[a-z]` que peude haber 1 o más `+`; que de ahí seria el primer nombre. 
Luego que hay un espacio `\s`, luego vuelvo a miarar que la primera letra es mayuscula `[A-Z]`, luego miro las letras minusculas y que haya 1 o más `[a-záéíóúñ]+` y miro que si tiene segunde o nombre, osea que sea opcional `?`. De ahí miro que haya espacio o no `\s?`, ya que algunos tienen solo 1 nombre yluego apellido el espacio ya se tome encuenta del primer nombre de la expresion regular. De ahí miro si hay o no segundo apellido o más `.*` y para validadrlo con lineac ompleta agrego al inicio `^` y al final `$` de la expresion regular.

    ^[A-ZÓ][a-zzáéíóúñ]+\s[A-Z][a-záéíóúñ]+?\s?.*$


# Búsqueda y reemplazo

Al igual que una navaja suiza, las expresiones regulares son una herramienta increíblemente útil pero tienes que darle la importancia y las responsabilidades adecuadas a cada una, ya que no son la panacea, no solucionan todos los problemas.

El uso más conveniente de las expresiones regulares es buscar coincidencias o matches de cadenas en un texto, y si es necesario, reemplazarlas con un texto diferente.


----------------------------------------------------------------
<b>Los paréntesis ()</b> sirven para agrupar caracteres. Tiene algunas diferencias notables a los paréntesis cuadrados.
Entre ellas se puede usar caracteres especiales conservan su significado dentro de los paréntesis.
Utilizando dentro del paréntesis una barra “|” podemos separar y hacer búsquedas similares. Ejemplo: (este|oeste|norte|sur)
-------------------------------------------------------------------

- No se si entendí bien. Según los grupos creados con los () son llamados según el símbolo de $n donde n es igual a la posición en que es invocada el grupo?

<p>Así es, si tenemos un listado de códigos como:</p>
<pre><code>ADF-<span class="hljs-number">012-415</span>
DBF-<span class="hljs-number">543-820</span>
</code></pre>
<p>suponiendo que fueran miles, y quisiéramos invertir su orden y reemplazar el guión por guión bajo, podríamos utilizar una expresión como:</p>
<pre><code>(<span class="hljs-tag">\<span class="hljs-name">w</span><span class="hljs-string">{3}</span></span>)-(<span class="hljs-tag">\<span class="hljs-name">d</span><span class="hljs-string">{3}</span></span>)-(<span class="hljs-tag">\<span class="hljs-name">d</span><span class="hljs-string">{3}</span></span>)
</code></pre>
<p>y luego la expresión de reemplazo:</p>
<pre><code>$3<span class="hljs-number">_</span>$2<span class="hljs-number">_</span>$1
</code></pre>
<p>obteniendo:</p>
<pre><code>415_012_ADF
820_543_DBF
</code></pre>
<p>Nos charlamos</p>

----------------------------------------------------------------------
# Uso de REGEX para descomponer querys GET

Al hacer consultas a sitios web mediante el método GET se envían todas las variables al servidor a través de la misma URL.

La parte de esta url que viene luego del signo de interrogación ? se le llama query del request que es: `variable1=valor1&variable2=valor2&...` y así tantas veces como se necesite. En esta clase veremos como extraer estas variables usando expresiones regulares.

------------------------------------------------------------

<p>Para aquellos que usan un editor como Visual Studio Code al querer hacer una nueva línea en el replace debemos colocar el salto de línea: \n</p>
<pre><code>Find: [<span class="hljs-string">\?&amp;</span>](<span class="hljs-link">\w+</span>)=([^&amp;\n]+)
Replace: \n - $1=$2
</code></pre>

----------------------------------------------------------------

# Banderas

<p>Las expresiones regulares pueden tener banderas que afectan la búsqueda, éstas deberán de estar hasta el final de la línea.</p>
<p><a href="https://javascript.info/regexp-introduction#flags" rel="nofollow noopener" target="_blank"><strong>Listado de Banderas en js:</strong></a><br>
<strong><code>i</code></strong><br>
Con este indicador, la búsqueda no distingue entre mayúsculas y minúsculas: no hay diferencia entre A y a<br>
<strong><code>g</code></strong><br>
Con esta marca, la búsqueda busca todas las coincidencias, sin ella, solo se devuelve la primera coincidencia.<br>
<strong><code>m</code></strong><br>
Modo multilínea<br>
<strong><code>s</code></strong><br>
Habilita el modo “dotall”, que permite un punto. para que coincida con el carácter de nueva línea \ n<br>
<strong><code>u</code></strong><br>
Permite el soporte completo de Unicode. La bandera permite el procesamiento correcto de pares sustitutos.<br>
<strong><code>y</code></strong><br>
Modo “adhesivo”: búsqueda en la posición exacta del texto</p>

# javascript

-------------------------------------------
<p>Algunas banderas de expresiones regulares en Javascript:</p>
<ul>
<li><strong>g</strong>: busca un patrón más de una vez.</li>
<li><strong>i</strong>: case insensitive (ignora mayúsculas y minúsculas).</li>
<li><strong>m</strong>: búsqueda en múltiples líneas.</li>
</ul>
<p>Pueden combinarse varias banderas.<br>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions" rel="nofollow" target="_blank">Expresiones Regulares en Javascript</a></p>
-------------------------------------

# `grep` y `find` desde consola

En los sistemas operativos basados en UNIX podemos utilizar expresiones regulares a través de la consola mediante los comandos grep y find.

grep: Nos ayuda a buscar dentro de los archivos, textos muy puntuales utilizando una versión muy reducida de expresiones regulares.

find: Se utiliza para encontrar archivos en un determinado directorio a partir de diversas reglas de búsqueda que incluyen expresiones regulares.


--------------------------------------
<p>Para usar <strong>expresiones regulares</strong> en <strong>Ubuntu 16.04</strong> se necesita agregar la flag <strong>-E</strong> o <strong>-G</strong>:</p>
<pre><code><span class="hljs-keyword">cat</span> results.csv | grep -<span class="hljs-keyword">E</span> ,3[0-9],
</code></pre>

- Tambien sirve escapando los corchetes.

    cat results.csv | grep ,3\[0-9\],

------------------------------------------------
<p>En ArchLinux usando zsh (Z shell) tuve que usar comillas simples para que funcionaran las expresiones regulares:</p>
<pre><code><span class="hljs-keyword">cat</span> results.csv | <span class="hljs-keyword">grep</span> <span class="hljs-string">',3[0-9],'</span> 
</code></pre>
-----------------------------------------------------

![image ver](images/ex10.png)