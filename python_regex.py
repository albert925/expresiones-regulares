import re

pattern = re.compile(r'^([\d]{4,4})\-\d\d-\d\d,(.+),(.+),(\d+),(\d+),.*$')

# 2018-06-04,Italy,Netherlands,1,1,Friendly,Turin,Italy,FALSE
with open('./files/results.csv', 'r', encoding='utf-8') as f:
    for line in f:
        res = re.match(pattern, line)
        if res:
            total = int(res.group(4)) + int(res.group(5))
            if total > 10:
                print(f'goles: {total}, {res.group(1)} {res.group(2)} - {res.group(3)}, {res.group(4)} - {int(res.group(5))}')
    f.close()
